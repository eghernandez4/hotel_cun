json.extract! recibo, :id, :rec_id, :rec_confirmpago, :grup_id, :cur_id, :est_cedula, :doc_cedula, :created_at, :updated_at
json.url recibo_url(recibo, format: :json)
