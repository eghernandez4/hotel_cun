json.extract! certificado, :id, :cert_id, :cert_fechaexpedicion, :grup_id, :cur_id, :est_cedula, :doc_cedula, :created_at, :updated_at
json.url certificado_url(certificado, format: :json)
