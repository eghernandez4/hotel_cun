json.extract! curso, :id, :cur_id, :cur_nombre, :cur_tituloaobtener, :cur_tipo, :cur_fechainicio, :cur_fechafin, :cur_horainicio, :cur_horafin, :cur_costoinscripcion, :doc_cedula, :created_at, :updated_at
json.url curso_url(curso, format: :json)
