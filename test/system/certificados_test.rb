require "application_system_test_case"

class CertificadosTest < ApplicationSystemTestCase
  setup do
    @certificado = certificados(:one)
  end

  test "visiting the index" do
    visit certificados_url
    assert_selector "h1", text: "Certificados"
  end

  test "creating a Certificado" do
    visit certificados_url
    click_on "New Certificado"

    fill_in "Cert fechaexpedicion", with: @certificado.cert_fechaexpedicion
    fill_in "Cert", with: @certificado.cert_id
    fill_in "Cur", with: @certificado.cur_id
    fill_in "Doc cedula", with: @certificado.doc_cedula
    fill_in "Est cedula", with: @certificado.est_cedula
    fill_in "Grup", with: @certificado.grup_id
    click_on "Create Certificado"

    assert_text "Certificado was successfully created"
    click_on "Back"
  end

  test "updating a Certificado" do
    visit certificados_url
    click_on "Edit", match: :first

    fill_in "Cert fechaexpedicion", with: @certificado.cert_fechaexpedicion
    fill_in "Cert", with: @certificado.cert_id
    fill_in "Cur", with: @certificado.cur_id
    fill_in "Doc cedula", with: @certificado.doc_cedula
    fill_in "Est cedula", with: @certificado.est_cedula
    fill_in "Grup", with: @certificado.grup_id
    click_on "Update Certificado"

    assert_text "Certificado was successfully updated"
    click_on "Back"
  end

  test "destroying a Certificado" do
    visit certificados_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Certificado was successfully destroyed"
  end
end
