require "application_system_test_case"

class CursosTest < ApplicationSystemTestCase
  setup do
    @curso = cursos(:one)
  end

  test "visiting the index" do
    visit cursos_url
    assert_selector "h1", text: "Cursos"
  end

  test "creating a Curso" do
    visit cursos_url
    click_on "New Curso"

    fill_in "Cur costoinscripcion", with: @curso.cur_costoinscripcion
    fill_in "Cur fechafin", with: @curso.cur_fechafin
    fill_in "Cur fechainicio", with: @curso.cur_fechainicio
    fill_in "Cur horafin", with: @curso.cur_horafin
    fill_in "Cur horainicio", with: @curso.cur_horainicio
    fill_in "Cur", with: @curso.cur_id
    fill_in "Cur nombre", with: @curso.cur_nombre
    fill_in "Cur tipo", with: @curso.cur_tipo
    fill_in "Cur tituloaobtener", with: @curso.cur_tituloaobtener
    fill_in "Doc cedula", with: @curso.doc_cedula
    click_on "Create Curso"

    assert_text "Curso was successfully created"
    click_on "Back"
  end

  test "updating a Curso" do
    visit cursos_url
    click_on "Edit", match: :first

    fill_in "Cur costoinscripcion", with: @curso.cur_costoinscripcion
    fill_in "Cur fechafin", with: @curso.cur_fechafin
    fill_in "Cur fechainicio", with: @curso.cur_fechainicio
    fill_in "Cur horafin", with: @curso.cur_horafin
    fill_in "Cur horainicio", with: @curso.cur_horainicio
    fill_in "Cur", with: @curso.cur_id
    fill_in "Cur nombre", with: @curso.cur_nombre
    fill_in "Cur tipo", with: @curso.cur_tipo
    fill_in "Cur tituloaobtener", with: @curso.cur_tituloaobtener
    fill_in "Doc cedula", with: @curso.doc_cedula
    click_on "Update Curso"

    assert_text "Curso was successfully updated"
    click_on "Back"
  end

  test "destroying a Curso" do
    visit cursos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Curso was successfully destroyed"
  end
end
