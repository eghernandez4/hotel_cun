require 'test_helper'

class CursosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @curso = cursos(:one)
  end

  test "should get index" do
    get cursos_url
    assert_response :success
  end

  test "should get new" do
    get new_curso_url
    assert_response :success
  end

  test "should create curso" do
    assert_difference('Curso.count') do
      post cursos_url, params: { curso: { cur_costoinscripcion: @curso.cur_costoinscripcion, cur_fechafin: @curso.cur_fechafin, cur_fechainicio: @curso.cur_fechainicio, cur_horafin: @curso.cur_horafin, cur_horainicio: @curso.cur_horainicio, cur_id: @curso.cur_id, cur_nombre: @curso.cur_nombre, cur_tipo: @curso.cur_tipo, cur_tituloaobtener: @curso.cur_tituloaobtener, doc_cedula: @curso.doc_cedula } }
    end

    assert_redirected_to curso_url(Curso.last)
  end

  test "should show curso" do
    get curso_url(@curso)
    assert_response :success
  end

  test "should get edit" do
    get edit_curso_url(@curso)
    assert_response :success
  end

  test "should update curso" do
    patch curso_url(@curso), params: { curso: { cur_costoinscripcion: @curso.cur_costoinscripcion, cur_fechafin: @curso.cur_fechafin, cur_fechainicio: @curso.cur_fechainicio, cur_horafin: @curso.cur_horafin, cur_horainicio: @curso.cur_horainicio, cur_id: @curso.cur_id, cur_nombre: @curso.cur_nombre, cur_tipo: @curso.cur_tipo, cur_tituloaobtener: @curso.cur_tituloaobtener, doc_cedula: @curso.doc_cedula } }
    assert_redirected_to curso_url(@curso)
  end

  test "should destroy curso" do
    assert_difference('Curso.count', -1) do
      delete curso_url(@curso)
    end

    assert_redirected_to cursos_url
  end
end
