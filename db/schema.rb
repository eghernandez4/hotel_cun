# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_07_225717) do

  create_table "certificados", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "cert_id"
    t.date "cert_fechaexpedicion"
    t.integer "grup_id"
    t.integer "cur_id"
    t.integer "est_cedula"
    t.integer "doc_cedula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cursos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "cur_id"
    t.string "cur_nombre"
    t.string "cur_tituloaobtener"
    t.string "cur_tipo"
    t.date "cur_fechainicio"
    t.date "cur_fechafin"
    t.time "cur_horainicio"
    t.time "cur_horafin"
    t.integer "cur_costoinscripcion"
    t.integer "doc_cedula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grupos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "grup_id"
    t.integer "cur_id"
    t.integer "est_cedula"
    t.integer "doc_cedula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recibos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "rec_id"
    t.boolean "rec_confirmpago"
    t.integer "grup_id"
    t.integer "cur_id"
    t.integer "est_cedula"
    t.integer "doc_cedula"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "cedula"
    t.string "nombre"
    t.string "apellido"
    t.string "direccion"
    t.integer "celular"
    t.integer "telefono"
    t.integer "rol"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_usuarios_on_email", unique: true
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true
  end

end
