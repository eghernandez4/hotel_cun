class CreateGrupos < ActiveRecord::Migration[5.2]
  def change
    create_table :grupos do |t|
      t.integer :grup_id
      t.integer :cur_id
      t.integer :est_cedula
      t.integer :doc_cedula

      t.timestamps
    end
  end
end
