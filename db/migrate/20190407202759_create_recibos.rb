class CreateRecibos < ActiveRecord::Migration[5.2]
  def change
    create_table :recibos do |t|
      t.integer :rec_id
      t.boolean :rec_confirmpago
      t.integer :grup_id
      t.integer :cur_id
      t.integer :est_cedula
      t.integer :doc_cedula

      t.timestamps
    end
  end
end
