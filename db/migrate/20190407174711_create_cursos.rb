class CreateCursos < ActiveRecord::Migration[5.2]
  def change
    create_table :cursos do |t|
      t.integer :cur_id
      t.string :cur_nombre
      t.string :cur_tituloaobtener
      t.string :cur_tipo
      t.date :cur_fechainicio
      t.date :cur_fechafin
      t.time :cur_horainicio
      t.time :cur_horafin
      t.integer :cur_costoinscripcion
      t.integer :doc_cedula

      t.timestamps
    end
  end
end
