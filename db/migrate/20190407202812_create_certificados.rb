class CreateCertificados < ActiveRecord::Migration[5.2]
  def change
    create_table :certificados do |t|
      t.integer :cert_id
      t.date :cert_fechaexpedicion
      t.integer :grup_id
      t.integer :cur_id
      t.integer :est_cedula
      t.integer :doc_cedula

      t.timestamps
    end
  end
end
