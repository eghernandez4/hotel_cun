Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :usuarios
  root to: 'welcome#index'
  resources :certificados
  resources :recibos
  resources :grupos
  resources :cursos
   # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
